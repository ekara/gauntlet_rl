#include "camera.h"

void camera_init(Camera2D *camera, Entity *target, float zoom, CameraType type)
{
    camera->target = (Vector2) { target->position.x + (target->size.x*0.5f), target->position.y + (target->size.y*0.5f)};
    camera->offset = (Vector2) { GetRenderWidth()*0.5f, GetRenderHeight()*0.5f};
    camera->rotation = 0.0f;
    camera->zoom = zoom;
}

void camera_update(Camera2D *camera, Entity *target, float world_width, float world_height)
{
    camera->target = (Vector2) { target->position.x + (target->size.x*0.5f), target->position.y + (target->size.y*0.5f)};
    camera->offset = (Vector2) { GetRenderWidth()*0.5f, GetRenderHeight()*0.5f};
    Vector2 max = GetWorldToScreen2D((Vector2){ world_width, world_height}, *camera);
    Vector2 min = GetWorldToScreen2D((Vector2){ 0.0f, 0.0f }, *camera);
    if (max.x < GetRenderWidth()) camera->offset.x = GetRenderWidth() - (max.x - GetRenderWidth()/2);
    if (max.y < GetRenderHeight()) camera->offset.y = GetRenderHeight() - (max.y - GetRenderHeight()/2);
    if (min.x > 0) camera->offset.x = GetRenderWidth()/2 - min.x;
    if (min.y > 0) camera->offset.y = GetRenderHeight()/2 - min.y;
}