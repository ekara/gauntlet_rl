#include "game.h"

#define MAX_NUM_MENU_OPTIONS 4
const char SETTINGS_TITLE_STRING[20] = "Settings";
const char SETTINGS_OPTIONS_STRINGS[MAX_NUM_MENU_OPTIONS][15] = {
    "Back",
    "Volume",
    "Resolution",
    "Fullscreen"
};
static uint8_t selected_menu_option;
GameResolution current_resolution;

void scene_settings_init(void)
{
    selected_menu_option = 0;
}

void scene_settings_update(Game *game)
{
    scene_settings_input(game);
}

void scene_settings_render(Game *game)
{
    BeginDrawing();
        ClearBackground(BLACK);
        
        DrawText(SETTINGS_TITLE_STRING, 50, 50, 100, LIGHTGRAY);
        // Draw menu options
        for (int menu_option = 0; menu_option < MAX_NUM_MENU_OPTIONS; menu_option++)
        {
            DrawText(SETTINGS_OPTIONS_STRINGS[menu_option], 50, GetScreenHeight()-300 + menu_option*50, 50, menu_option == selected_menu_option ? WHITE : GRAY);
        }
        // Draw menu options state
        for (int menu_option = 1; menu_option < MAX_NUM_MENU_OPTIONS; menu_option++)
        {
            if (menu_option == 1)
            {
                DrawText(TextFormat("<%i>", game->volume), 500, GetScreenHeight()-300 + menu_option*50, 50, menu_option == selected_menu_option ? WHITE : GRAY);
            }
            else if (menu_option == 2)
            {
                char *resolution;
                if (game->game_resolution == GAME_RESOLUTION_HD)
                    resolution = "<1280x720>";
                else if (game->game_resolution == GAME_RESOLUTION_FHD)
                    resolution = "<1920x1080>";
                // else if (current_resolution == GAME_RESOLUTION_QHD)
                //     resolution = "<2560x1440>";
                // else if (current_resolution == GAME_RESOLUTION_UHD)
                //     resolution = "<3840x2160>";
                DrawText(resolution, 500, GetScreenHeight()-300 + menu_option*50, 50, menu_option == selected_menu_option ? WHITE : GRAY);
            }
            else if (menu_option == 3)
            {
                DrawText(TextFormat("<%s>", game->fullscreen ? "Yes" : "No"), 500, GetScreenHeight()-300 + menu_option*50, 50, menu_option == selected_menu_option ? WHITE : GRAY);
            }
        }
    EndDrawing();
}

void scene_settings_input(Game *game)
{
    if (IsKeyPressed(KEY_UP) || IsKeyPressed(KEY_W))
    {
        if (selected_menu_option == 0)
        {
            selected_menu_option = MAX_NUM_MENU_OPTIONS-1;
        }
        else
        {
            selected_menu_option--;
        }
    }
    if (IsKeyPressed(KEY_DOWN) || IsKeyPressed(KEY_S))
    {
        if (selected_menu_option == MAX_NUM_MENU_OPTIONS-1)
        {
            selected_menu_option = 0;
        }
        else
        {
            selected_menu_option++;
        }
    }
    if (IsKeyPressed(KEY_ENTER))
    {
        // TODO: Change scene
        if (selected_menu_option == 0)
        {
            game_set_scene(GAME_SCENE_MENU);
        }
    }
    if (IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_A))
    {
        if (selected_menu_option == 1)
        {
            game->volume--;
            if (game->volume < 0)
            {
                game->volume = 0;
            }
        }
        else if (selected_menu_option == 2)
        {
            if (current_resolution == 0)
            {
                current_resolution = GAME_RESOLUTION_MAX-1;
            }
            else
            {
                current_resolution--;
            }
            
            game_set_resolution(current_resolution);
        }
        else if (selected_menu_option == 3)
        {
            game_set_fullscreen();
        }
    }
    if (IsKeyPressed(KEY_RIGHT) || IsKeyPressed(KEY_D))
    {
        if (selected_menu_option == 1)
        {
            game->volume++;
            if (game->volume > 100)
            {
                game->volume = 100;
            }
        }
        else if (selected_menu_option == 2)
        {
            if (current_resolution == GAME_RESOLUTION_MAX-1)
            {
                current_resolution = 0;
            }
            else
            {
                current_resolution++;
            }

            game_set_resolution(current_resolution);
        }
        else if (selected_menu_option == 3)
        {
            game_set_fullscreen();
        }
    }
    if (IsKeyPressed(KEY_ESCAPE))
    {
        game_set_scene(GAME_SCENE_MENU);
    }
}