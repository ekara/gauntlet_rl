#include <stdio.h>
#include <stdlib.h>
// #include <dirent.h>
#include <string.h>

#include "game.h"

#define RAYGUI_IMPLEMENTATION 
#include "raygui.h"

EntityType solid_tiles[65] = {
    ENTITY_TYPE_SOLID_TREE_1,
    ENTITY_TYPE_SOLID_TREE_2,
    ENTITY_TYPE_SOLID_TREE_3,
    ENTITY_TYPE_SOLID_TREE_4,
    ENTITY_TYPE_SOLID_TREE_5,
    ENTITY_TYPE_SOLID_TREE_6,
    ENTITY_TYPE_SOLID_TREE_7,
    ENTITY_TYPE_SOLID_TREE_8,
    ENTITY_TYPE_SOLID_TREE_9,
    ENTITY_TYPE_SOLID_TREE_10,
    ENTITY_TYPE_SOLID_TREE_11,
    ENTITY_TYPE_SOLID_TREE_12,
    ENTITY_TYPE_SOLID_TREE_13,
    ENTITY_TYPE_SOLID_TREE_14,
    ENTITY_TYPE_SOLID_TREE_15,
    ENTITY_TYPE_SOLID_STONE,
    ENTITY_TYPE_SOLID_WATER_1,
    ENTITY_TYPE_SOLID_WATER_2,
    ENTITY_TYPE_SOLID_WATER_3,
    ENTITY_TYPE_SOLID_WATER_4,
    ENTITY_TYPE_SOLID_WATER_5,
    ENTITY_TYPE_SOLID_WATER_6,
    ENTITY_TYPE_SOLID_WATER_7,
    ENTITY_TYPE_SOLID_WATER_8,
    ENTITY_TYPE_SOLID_WATER_9,
    ENTITY_TYPE_SOLID_PIPE_1,
    ENTITY_TYPE_SOLID_PIPE_2,
    ENTITY_TYPE_SOLID_PIPE_3,
    ENTITY_TYPE_SOLID_PIPE_4,
    ENTITY_TYPE_SOLID_WALL_1,
    ENTITY_TYPE_SOLID_WALL_2,
    ENTITY_TYPE_SOLID_WALL_3,
    ENTITY_TYPE_SOLID_WALL_4,
    ENTITY_TYPE_SOLID_WALL_5,
    ENTITY_TYPE_SOLID_WALL_6,
    ENTITY_TYPE_SOLID_WALL_7,
    ENTITY_TYPE_SOLID_WALL_8,
    ENTITY_TYPE_SOLID_WALL_9,
    ENTITY_TYPE_SOLID_WALL_10,
    ENTITY_TYPE_SOLID_WALL_11,
    ENTITY_TYPE_SOLID_WALL_12,
    ENTITY_TYPE_SOLID_WALL_13,
    ENTITY_TYPE_SOLID_WALL_14,
    ENTITY_TYPE_SOLID_WALL_15,
    ENTITY_TYPE_SOLID_WALL_16,
    ENTITY_TYPE_SOLID_WALL_17,
    ENTITY_TYPE_SOLID_WALL_18,
    ENTITY_TYPE_SOLID_WALL_19,
    ENTITY_TYPE_SOLID_WALL_20,
    ENTITY_TYPE_SOLID_WALL_21,
    ENTITY_TYPE_SOLID_WALL_22,
    ENTITY_TYPE_SOLID_WALL_23,
    ENTITY_TYPE_SOLID_WALL_24,
    ENTITY_TYPE_SOLID_WALL_25,
    ENTITY_TYPE_SOLID_WALL_26,
    ENTITY_TYPE_SOLID_WALL_27,
    ENTITY_TYPE_SOLID_WALL_28,
    ENTITY_TYPE_SOLID_WALL_29,
    ENTITY_TYPE_SOLID_WALL_30,
    ENTITY_TYPE_SOLID_WALL_31,
    ENTITY_TYPE_SOLID_GRAVE_1,
    ENTITY_TYPE_SOLID_GRAVE_1,
    ENTITY_TYPE_SOLID_SIGN_1,
    ENTITY_TYPE_SOLID_SIGN_2,
    ENTITY_TYPE_SOLID_SIGN_1
};

EntityType decoration_tiles[18] = {
    ENTITY_TYPE_DECORATION_GROUND_1,
    ENTITY_TYPE_DECORATION_GROUND_2,
    ENTITY_TYPE_DECORATION_GROUND_3,
    ENTITY_TYPE_DECORATION_GROUND_4,
    ENTITY_TYPE_DECORATION_GRASS_1,
    ENTITY_TYPE_DECORATION_GRASS_2,
    ENTITY_TYPE_DECORATION_GRASS_3,
    ENTITY_TYPE_DECORATION_STREET_1,
    ENTITY_TYPE_DECORATION_STREET_2,
    ENTITY_TYPE_DECORATION_STREET_3,
    ENTITY_TYPE_DECORATION_STREET_4,
    ENTITY_TYPE_DECORATION_STREET_5,
    ENTITY_TYPE_DECORATION_BONES_1,
    ENTITY_TYPE_DECORATION_BONES_2,
    ENTITY_TYPE_DECORATION_WEB,
    ENTITY_TYPE_DECORATION_TORCH_1,
    ENTITY_TYPE_DECORATION_TORCH_2,
    ENTITY_TYPE_DECORATION_TORCH_3
};

EntityType object_tiles[10] = {
    ENTITY_TYPE_OBJECT_FOOD_1,
    ENTITY_TYPE_OBJECT_FOOD_2,
    ENTITY_TYPE_OBJECT_FOOD_3,
    ENTITY_TYPE_OBJECT_FOOD_4,
    ENTITY_TYPE_OBJECT_FOOD_5,
    ENTITY_TYPE_OBJECT_FOOD_6,
    ENTITY_TYPE_OBJECT_FOOD_7,
    ENTITY_TYPE_OBJECT_JEWEL_1,
    ENTITY_TYPE_OBJECT_JEWEL_2,
    ENTITY_TYPE_OBJECT_JEWEL_3
};

EntityType enemy_tiles[18] = {
    ENTITY_TYPE_ENEMY_SCORPION,
    ENTITY_TYPE_ENEMY_CRAB,
    ENTITY_TYPE_ENEMY_BEE,
    ENTITY_TYPE_ENEMY_TURTLE,
    ENTITY_TYPE_ENEMY_SPIDER_1,
    ENTITY_TYPE_ENEMY_SPIDER_2,
    ENTITY_TYPE_ENEMY_SPIDER_3,
    ENTITY_TYPE_ENEMY_GHOST_1,
    ENTITY_TYPE_ENEMY_GHOST_2,
    ENTITY_TYPE_ENEMY_SKELETON,
    ENTITY_TYPE_ENEMY_GOLEM,
    ENTITY_TYPE_ENEMY_SKULL,
    ENTITY_TYPE_ENEMY_OCTOPUS,
    ENTITY_TYPE_ENEMY_BAT,
    ENTITY_TYPE_ENEMY_SNAKE,
    ENTITY_TYPE_ENEMY_ALLIGATOR,
    ENTITY_TYPE_ENEMY_BEAR,
    ENTITY_TYPE_ENEMY_MAGE
};

static uint32_t selected_texture_id;
static uint32_t selected_entity_x;
static uint32_t selected_entity_y;
static uint8_t dropdown_active;
static Entity world_map[MAX_MAP_HEIGHT][MAX_MAP_WIDTH] = {0};

// main gui element to hold them all
static bool draw_gui;
static Rectangle gui_window_box = {0};
static Rectangle gui_padding_box = {0};
// file settings related elements
static bool file_changed;
static Rectangle file_control_panel = {0};
static Rectangle current_file_text = {0};
static Rectangle file_status_text = {0};
static Rectangle file_name_text_box = {0};
static char level_file_name[64] = "";
static bool edit_level_file_name;
static Rectangle save_level_button = {0};
static Rectangle new_level_button = {0};
// editor mode settings
static Rectangle editor_mode_panel = {0};
static int active_editor_mode;
static const char *editor_modes = "Place;Delete;Select";
static Rectangle editor_modes_text = {0};
static Rectangle editor_modes_togglegroup = {0};
static bool delete_mode;
static Rectangle editor_draw_grid_checkbox = {0};
static bool draw_grid;
// tile editing related elements
static Rectangle tile_selection_panel = {0};
static int active_tile_category;
static int prev_active_tile_category;
static const char *tile_categories = "Solid;Decoration;Enemies;Objects;Player";
static Rectangle tile_category_togglegroup = {0};
static Rectangle tile_scroll_panel = {0};
static Vector2 scroll = {0};
static Rectangle view = {0};
// static int scroll_index = 0;
// entity settings related elements
static Rectangle entity_group_box = {0};
// static Rectangle entity_texture = {0};
static Rectangle entity_type = {0};
static Rectangle entity_texture_id = {0};
static Rectangle entity_position = {0};
static Rectangle entity_rotation = {0};
static Rectangle entity_rotation_combobox = {0};
static const char *rotation_combobox_options = "0.0 deg;90.0 deg;180.0 deg;270.0 deg";
static int active_rotation_option;
static Rectangle entity_flipped = {0};
static Rectangle entity_flipped_combobox = {0};
static const char *flipped_combobox_options = "No;Yes";
static int active_flipped_option;
// messagebox related elements
static bool unsaved_msg_box;
static bool missing_filename_msg_box;
static bool overriding_msg_box;
static bool override_file;

static Camera2D camera = {0};


void scene_editor_init(void)
{
    // camera init
    camera.offset = (Vector2){0.0f, 0.0f};
    camera.target = (Vector2){0.0f, 0.0f};
    camera.rotation = 0.0f;
    camera.zoom = 3.0f;

    draw_gui = true;
    draw_grid = true;
    active_editor_mode = 0;
    active_tile_category = 0;
    prev_active_tile_category = 0;
    active_rotation_option = 0;
    active_flipped_option = 0;
    file_changed = false;
    level_file_name[0] = '\0';
    edit_level_file_name = false;
    delete_mode = false;
    selected_texture_id = 0;
    selected_entity_x = 0;
    selected_entity_y = 0;
    dropdown_active = 0;
    unsaved_msg_box = false;
    missing_filename_msg_box = false;
    overriding_msg_box = false;
    override_file = false;

    // gui settings
    gui_window_box.x = 0.0f;
    gui_window_box.y = 0.0f;
    gui_window_box.width = GetRenderWidth()*0.3f;
    gui_window_box.height = GetRenderHeight();
    // 
    gui_padding_box.x = gui_window_box.x + 10.0f;
    gui_padding_box.y = gui_window_box.y + 30.0f;
    gui_padding_box.width = gui_window_box.width - gui_padding_box.x*2;
    gui_padding_box.height = gui_window_box.height - gui_padding_box.y*2;
    //
    file_status_text.x = gui_padding_box.x;
    file_status_text.y = gui_padding_box.y;
    file_status_text.width = gui_padding_box.width;
    file_status_text.height = 20.0f;
    // 
    file_control_panel.x = gui_padding_box.x;
    file_control_panel.y = file_status_text.y + 20;
    file_control_panel.width = gui_padding_box.width;
    file_control_panel.height = 50.0f;
    current_file_text.x = file_control_panel.x + 5.0f;
    current_file_text.y = file_control_panel.y + 15.0f;
    current_file_text.width = file_control_panel.width*0.15f;
    current_file_text.height = 20.0f;
    file_name_text_box.x = current_file_text.x + current_file_text.width;
    file_name_text_box.y = current_file_text.y;
    file_name_text_box.width = file_control_panel.width*0.4f;
    file_name_text_box.height = 20.0f;
    save_level_button.x = file_name_text_box.x + file_name_text_box.width;
    save_level_button.y = current_file_text.y;
    save_level_button.width = file_control_panel.width*0.2f;
    save_level_button.height = 20.0f;
    new_level_button.x = save_level_button.x + save_level_button.width;
    new_level_button.y = current_file_text.y;
    new_level_button.width = file_control_panel.width*0.2f;
    new_level_button.height = 20.0f;
    //
    editor_mode_panel.x = gui_padding_box.x;
    editor_mode_panel.y = file_control_panel.y + file_control_panel.height + 20.0f;
    editor_mode_panel.width = gui_padding_box.width;
    editor_mode_panel.height = 80.0f;
    editor_modes_text.x = editor_mode_panel.x + 5.0f;
    editor_modes_text.y = editor_mode_panel.y + 15.0f;
    editor_modes_text.width = editor_mode_panel.width*0.15f;
    editor_modes_text.height = 20.0f;
    editor_modes_togglegroup.x = editor_modes_text.x + editor_modes_text.width;
    editor_modes_togglegroup.y = editor_modes_text.y;
    editor_modes_togglegroup.width = (editor_mode_panel.width*0.6f)/3.0f;
    editor_modes_togglegroup.height = 20.0f;
    editor_draw_grid_checkbox.x = editor_mode_panel.x + 5.0f;
    editor_draw_grid_checkbox.y = editor_modes_text.y + editor_modes_text.height + 10.0f;
    editor_draw_grid_checkbox.width = 20.0f;
    editor_draw_grid_checkbox.height = 20.0f;
    // 
    tile_selection_panel.x = gui_padding_box.x;
    tile_selection_panel.y = editor_mode_panel.y + editor_mode_panel.height + 20.0f;
    tile_selection_panel.width = gui_padding_box.width;
    tile_selection_panel.height = 300.0f;
    tile_category_togglegroup.x = tile_selection_panel.x + 5.0f;
    tile_category_togglegroup.y = tile_selection_panel.y + 15.0f;
    tile_category_togglegroup.width = (tile_selection_panel.width-10.0f)/5.0f;
    tile_category_togglegroup.height = 20.0f;
    tile_scroll_panel.x = tile_selection_panel.x + 5.0f;
    tile_scroll_panel.y = tile_category_togglegroup.y + tile_category_togglegroup.height;
    tile_scroll_panel.width = tile_selection_panel.width-10.0f;
    tile_scroll_panel.height = 250.0f;
    // 
    entity_group_box.x = gui_padding_box.x;
    entity_group_box.y = tile_selection_panel.y + tile_selection_panel.height + 20.0f;
    entity_group_box.width = gui_padding_box.width;
    entity_group_box.height = 160.0f;
    entity_type.x = entity_group_box.x + 5.0f;
    entity_type.y = entity_group_box.y + 55.0f;
    entity_type.width = entity_group_box.width;
    entity_type.height = 20.0f;
    entity_texture_id.x = entity_group_box.x + 5.0f;
    entity_texture_id.y = entity_type.y + entity_type.height;
    entity_texture_id.width = entity_group_box.width;
    entity_texture_id.height = 20.0f;
    entity_position.x = entity_group_box.x + 5.0f;
    entity_position.y = entity_texture_id.y + entity_texture_id.height;
    entity_position.width = entity_group_box.width;
    entity_position.height = 20.0f;
    entity_rotation.x = entity_group_box.x + 5;
    entity_rotation.y = entity_position.y + entity_position.height;
    entity_rotation.width = entity_group_box.width*0.15f;
    entity_rotation.height = 20.0f;
    entity_rotation_combobox.x = entity_rotation.x + entity_rotation.width;
    entity_rotation_combobox.y = entity_rotation.y;
    entity_rotation_combobox.width = entity_group_box.width*0.4f;
    entity_rotation_combobox.height = 20.0f;
    entity_flipped.x = entity_group_box.x + 5;
    entity_flipped.y = entity_rotation.y + entity_rotation.height;
    entity_flipped.width = entity_group_box.width*0.15f;
    entity_flipped.height = 20.0f;
    entity_flipped_combobox.x = entity_flipped.x + entity_flipped.width;
    entity_flipped_combobox.y = entity_flipped.y;
    entity_flipped_combobox.width = entity_group_box.width*0.4f;
    entity_flipped_combobox.height = 20.0f;

    // init entities
    int id = 0;
    for (int y = 0; y < MAX_MAP_HEIGHT; y++)
    {
        for (int x = 0; x < MAX_MAP_WIDTH; x++)
        {
            world_map[y][x].id = id;
            world_map[y][x].type = ENTITY_TYPE_NONE;
            world_map[y][x].texture_id = ENTITY_TYPE_NONE;
            world_map[y][x].texture_flipped = 1.0f;
            world_map[y][x].position = (Vector2){.x = x*TILE_SIZE, .y = y*TILE_SIZE};
            world_map[y][x].origin = (Vector2){.x = TILE_SIZE * 0.5f, .y = TILE_SIZE * 0.5f};
            id++;
        }
    }
}

void load_empty_level()
{
    scene_editor_init();
}

void load_level_file()
{
    int data_size = 0;
    char fileNamePath[256] = "\0";
    char fileName[64] = "\0";
    char line[100];

    FilePathList dropped_files = LoadDroppedFiles();

    // Check file extensions for drag-and-drop
    if ((dropped_files.count == 1) && IsFileExtension(dropped_files.paths[0], ".txt"))
    {
        FILE *level_file = fopen(dropped_files.paths[0], "rb");

        int x = 0;
        int y = 0;
        while (fgets(line, sizeof(line), level_file) != NULL && y < 100)
        {
            if (sscanf(line, "%u %hu %f %f %f %f", &world_map[y][x].type, &world_map[y][x].texture_id, &world_map[y][x].texture_flipped, &world_map[y][x].position.x, &world_map[y][x].position.y, &world_map[y][x].rotation) == 6)
            {
                x++;
                if (x >= 100)
                {
                    y++;
                    x = 0;
                }
            }
            else
            {
                fprintf(stderr, "Error parsing line: %s", line);
            }
        }

        fseek(level_file, 0L, SEEK_END);
        data_size = ftell(level_file);
        printf("%d\n", data_size);
        fclose(level_file);
        
        // NOTE: Returned string is just a pointer to droppeddropped_filesFiles[0],
        // we need to make a copy of that data somewhere else: fileName
        strcpy(fileNamePath, dropped_files.paths[0]);
        strcpy(fileName, GetFileName(dropped_files.paths[0]));
        strcpy(level_file_name, GetFileNameWithoutExt(dropped_files.paths[0]));
        printf("%s\n", fileName);
        printf("%s\n", fileNamePath);
    }

    UnloadDroppedFiles(dropped_files);
}

void save_level_file()
{
    const char *working_directory = GetApplicationDirectory();
    const char *folder_path = "data\\levels\\";
    char full_path[100 + sizeof(folder_path)];
    snprintf(full_path, sizeof(full_path), "%s%s", working_directory, folder_path);
    char file_path[64 + sizeof(full_path) + 4];  // 4 for ".txt"s
    char file_name[64];
    strcpy(file_name, level_file_name);
    strcat(file_name, ".txt");
    snprintf(file_path, sizeof(file_path), "%s%s", full_path, file_name);

    if (level_file_name[0] == '\0')
    {
        missing_filename_msg_box = true;
    }
    else if (IsPathFile(file_path) && !override_file)
    {
        overriding_msg_box = true;
    }
    else
    {
        FILE *file = fopen(file_path, "w");
        // Write each line to the file
        for (int y = 0; y < MAX_MAP_HEIGHT; y++)
        {
            for (int x = 0; x < MAX_MAP_WIDTH; x++)
            {
                // SaveFileText(level_file_name, TextFormat("%i %i %f %f %f\n", world_map[y][x].type, world_map[y][x].texture_id, world_map[y][x].position.x, world_map[y][x].position.y, world_map[y][x].rotation));
                fprintf(file, "%u %hu %f %f %f %f\n", world_map[y][x].type, world_map[y][x].texture_id, world_map[y][x].texture_flipped, world_map[y][x].position.x, world_map[y][x].position.y, world_map[y][x].rotation);
            }
        }
        fclose(file);

        file_changed = false;
        override_file = false;
    }
}

void scene_editor_update(void)
{
    scene_editor_input();

    if (IsFileDropped())
    {
        load_level_file();
    }

    if (active_rotation_option == 0) world_map[selected_entity_y][selected_entity_x].rotation = 0.0f;
    else if (active_rotation_option == 1) world_map[selected_entity_y][selected_entity_x].rotation = 90.0f;
    else if (active_rotation_option == 2) world_map[selected_entity_y][selected_entity_x].rotation = 180.0f;
    else if (active_rotation_option == 3) world_map[selected_entity_y][selected_entity_x].rotation = 270.0f;

    if (active_flipped_option == 0) world_map[selected_entity_y][selected_entity_x].texture_flipped = 1.0f;
    else if (active_flipped_option == 1) world_map[selected_entity_y][selected_entity_x].texture_flipped = -1.0f;
}

bool is_mouse_over_gui()
{
    if (!draw_gui || GetMousePosition().x > gui_window_box.width)
    {
        return false;
    }
    // else if (unsaved_msg_box || missing_filename_msg_box || overriding_msg_box)
    // {
    //     if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){GetRenderWidth()*0.5f-100.0f, GetRenderHeight()*0.5f-100.0f, 400.0f, 200.0f}))
    //     {
    //         return true;
    //     }
    //     else
    //     {
    //         return false;
    //     }
    // }
    else
    {
        return true;
    }
}

void scene_editor_render(Texture2D *tilesheet)
{
    uint16_t num_tiles_horizontal = tilesheet->width / TILE_SIZE;
    uint16_t num_tiles_vertical = tilesheet->height / TILE_SIZE;

    BeginDrawing();
        ClearBackground(BLACK);
        
        camera.target.x = GetScreenWidth() * 0.5;
        camera.target.y = GetScreenHeight() * 0.5;

        BeginMode2D(camera);
            // Draw tilemap grid
            if (draw_grid)
            {
                for (int y = 0; y <= MAX_MAP_HEIGHT; y++)
                {
                    DrawLine(0, y*TILE_SIZE, MAX_MAP_WIDTH*TILE_SIZE, y*TILE_SIZE, GRAY);
                }
                for (int x = 0; x <= MAX_MAP_WIDTH; x++)
                {
                    DrawLine(x*TILE_SIZE, 0, x*TILE_SIZE, MAX_MAP_HEIGHT*TILE_SIZE, GRAY);
                }
            }
            else
            {
                DrawLine(0, 0*TILE_SIZE, MAX_MAP_WIDTH*TILE_SIZE, 0*TILE_SIZE, GRAY);
                DrawLine(0, MAX_MAP_HEIGHT*TILE_SIZE, MAX_MAP_WIDTH*TILE_SIZE, MAX_MAP_HEIGHT*TILE_SIZE, GRAY);
                DrawLine(0*TILE_SIZE, 0, 0*TILE_SIZE, MAX_MAP_HEIGHT*TILE_SIZE, GRAY);
                DrawLine(MAX_MAP_WIDTH*TILE_SIZE, 0, MAX_MAP_WIDTH*TILE_SIZE, MAX_MAP_HEIGHT*TILE_SIZE, GRAY);
            }

            // Draw the level with all its entities
            for (uint8_t y = 0; y < MAX_MAP_HEIGHT; y++)
            {
                for (uint8_t x = 0; x < MAX_MAP_WIDTH; x++)
                {
                    entity_draw(tilesheet, &world_map[y][x]);
                    
                    Vector2 mouse_position = GetScreenToWorld2D(GetMousePosition(), camera);
                    if (mouse_position.x < (x*TILE_SIZE+TILE_SIZE) && mouse_position.x > (x*TILE_SIZE) && mouse_position.y < (y*TILE_SIZE+TILE_SIZE) && mouse_position.y >= (y*TILE_SIZE))
                    {
                        if (active_editor_mode == 0)
                        {
                            DrawTexturePro(
                                *tilesheet, 
                                (Rectangle){TILE_SIZE*(selected_texture_id % num_tiles_horizontal), TILE_SIZE * floor(selected_texture_id / num_tiles_horizontal), TILE_SIZE, TILE_SIZE}, 
                                (Rectangle){x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE}, 
                                (Vector2){0.0f, 0.0f}, 
                                0.0f, 
                                WHITE
                            );
                        }
                        else if (active_editor_mode == 1)
                        {
                            DrawRectangle(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE, ColorAlpha(RED, 0.5f));
                        }
                        else if (active_editor_mode == 2)
                        {
                            DrawRectangle(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE, ColorAlpha(LIGHTGRAY, 0.5f));
                        }

                        if (IsMouseButtonDown(MOUSE_BUTTON_LEFT) && !unsaved_msg_box && !missing_filename_msg_box && !overriding_msg_box)
                        {
                            if (is_mouse_over_gui()) continue;

                            if (active_editor_mode == 0)
                            {
                                if (world_map[y][x].type != selected_texture_id)
                                {
                                    world_map[y][x].type = selected_texture_id;
                                    world_map[y][x].texture_id = selected_texture_id;
                                    world_map[y][x].texture_flipped = 1.0f;
                                    world_map[y][x].rotation = 0.0f;
                                }
                            }
                            else if (active_editor_mode == 1)
                            {
                                world_map[y][x].type = ENTITY_TYPE_NONE;
                                world_map[y][x].texture_id = ENTITY_TYPE_NONE;
                            }
                            else if (active_editor_mode == 2)
                            {
                                selected_entity_x = x;
                                selected_entity_y = y;
                                if (world_map[selected_entity_y][selected_entity_x].rotation == 0.0f)
                                {
                                    active_rotation_option = 0;
                                }
                                else if (world_map[selected_entity_y][selected_entity_x].rotation == 90.0f)
                                {
                                    active_rotation_option = 1;
                                }
                                else if (world_map[selected_entity_y][selected_entity_x].rotation == 180.0f)
                                {
                                    active_rotation_option = 2;
                                }
                                else if (world_map[selected_entity_y][selected_entity_x].rotation == 270.0f)
                                {
                                    active_rotation_option = 3;
                                }

                                if (world_map[selected_entity_y][selected_entity_x].texture_flipped == 1.0f)
                                {
                                    active_flipped_option = 0;
                                }
                                else if (world_map[selected_entity_y][selected_entity_x].texture_flipped == -1.0f)
                                {
                                    active_flipped_option = 1;
                                }
                            }

                            file_changed = true;
                        }
                        else if (IsMouseButtonPressed(MOUSE_BUTTON_RIGHT) && !unsaved_msg_box && !missing_filename_msg_box && !overriding_msg_box)
                        {
                            if (is_mouse_over_gui()) continue;

                            if (active_editor_mode == 0)
                            {
                                selected_texture_id = world_map[y][x].type;
                            }
                            else if (active_editor_mode == 2)
                            {
                                selected_entity_x = x;
                                selected_entity_y = y;
                                if (world_map[selected_entity_y][selected_entity_x].rotation == 0.0f)
                                {
                                    active_rotation_option = 0;
                                }
                                else if (world_map[selected_entity_y][selected_entity_x].rotation == 90.0f)
                                {
                                    active_rotation_option = 1;
                                }
                                else if (world_map[selected_entity_y][selected_entity_x].rotation == 180.0f)
                                {
                                    active_rotation_option = 2;
                                }
                                else if (world_map[selected_entity_y][selected_entity_x].rotation == 270.0f)
                                {
                                    active_rotation_option = 3;
                                }

                                active_rotation_option++;
                                if (active_rotation_option >= 4)
                                {
                                    active_rotation_option = 0;
                                }
                            }
                            file_changed = true;
                        }
                    }
                }
            }
        EndMode2D();

        if (draw_gui)
        {
            GuiWindowBox(gui_window_box, "Editor");

            GuiSetStyle(BUTTON, BASE_COLOR_NORMAL, 0xc9c9c9ff);
            GuiSetStyle(BUTTON, BASE_COLOR_FOCUSED, 0xc9effeff);
            GuiSetStyle(BUTTON, BORDER_WIDTH, 1);

            if (file_changed) GuiDrawText("LEVEL CHANGED", file_status_text, TEXT_ALIGN_RIGHT, RED);
            else if (!file_changed) GuiDrawText("NOTHING TO SAVE", file_status_text, TEXT_ALIGN_RIGHT, BLACK);

            GuiGroupBox(file_control_panel, "file");
            GuiDrawText("file: ", current_file_text, TEXT_ALIGN_LEFT, BLACK);
            if (GuiTextBox(file_name_text_box, level_file_name, 64, edit_level_file_name))
            {
                edit_level_file_name = !edit_level_file_name;
            }
            if (GuiButton(save_level_button, "Save Level"))
            {
                save_level_file();
            }
            if (missing_filename_msg_box)
            {
                int pressed_btn = GuiMessageBox((Rectangle){GetRenderWidth()*0.5f-100.0f, GetRenderHeight()*0.5f-100.0f, 400.0f, 200.0f}, "Warning", "File name missing.\nEnter name to save level.", "Okay");
                if (pressed_btn == 1)
                {
                    missing_filename_msg_box = false;
                }
            }
            if (overriding_msg_box)
            {
                int pressed_btn = GuiMessageBox((Rectangle){GetRenderWidth()*0.5f-100.0f, GetRenderHeight()*0.5f-100.0f, 400.0f, 200.0f}, "Warning", "Level with that name exists already.", "Override;Cancel");
                if (pressed_btn == 1)
                {
                    overriding_msg_box = false;
                    override_file = true;
                    save_level_file();
                }
                else if (pressed_btn == 2)
                {
                    overriding_msg_box = false;
                }
            }
            if (GuiButton(new_level_button, "New Level")) 
            {
                if (file_changed)
                {
                    unsaved_msg_box = true;
                }
                else
                {
                    load_empty_level();
                }
                
            }
            if (unsaved_msg_box)
            {
                int pressed_btn = GuiMessageBox((Rectangle){GetRenderWidth()*0.5f-100.0f, GetRenderHeight()*0.5f-100.0f, 400.0f, 200.0f}, "New Map", "You are about to create a new level.\nUnsaved changes will be lost.", "Save;Discard");
                if (pressed_btn == 0)
                {
                    unsaved_msg_box = false;
                }
                else if (pressed_btn == 1)
                {
                    save_level_file();
                    // load_empty_level();
                    unsaved_msg_box = false;
                }
                else if (pressed_btn == 2)
                {
                    load_empty_level();
                    unsaved_msg_box = false;
                }
            }

            GuiSetStyle(BUTTON, BASE_COLOR_NORMAL, 0x000000ff);
            GuiSetStyle(BUTTON, BASE_COLOR_FOCUSED, 0x555555ff);
            GuiSetStyle(BUTTON, BORDER_WIDTH, 0);
            GuiSetStyle(TOGGLE, GROUP_PADDING, 0);

            GuiGroupBox(editor_mode_panel, "editor");
            GuiLabel(editor_modes_text, "Mode: ");
            GuiToggleGroup(editor_modes_togglegroup, editor_modes, &active_editor_mode);
            GuiCheckBox(editor_draw_grid_checkbox, "Draw Grid", &draw_grid);

            GuiGroupBox(tile_selection_panel, "tilesheet");
            GuiToggleGroup(tile_category_togglegroup, tile_categories, &active_tile_category);
            if (prev_active_tile_category != active_tile_category)
            {
                prev_active_tile_category = active_tile_category;
                scroll.x = 0.0f;
                scroll.y = 0.0f;
            }
            // GuiScrollPanel(tile_scroll_panel, "tileset", (Rectangle){0, 0, num_tiles_horizontal * TILE_SIZE*2, num_tiles_vertical * TILE_SIZE*2}, &scroll, &view);
            GuiScrollPanel(tile_scroll_panel, "tileset", (Rectangle){0, 0, tile_scroll_panel.width, ceil((65/(floor(tile_scroll_panel.width / (TILE_SIZE*2)))))*TILE_SIZE*2}, &scroll, &view);
            BeginScissorMode(view.x, view.y, view.width, view.height);
                if (active_tile_category == 0)
                {
                    uint16_t num_tiles_x = floor(tile_scroll_panel.width / (TILE_SIZE*2));
                    uint16_t pos_x = 0;
                    uint16_t pos_y = 0;
                    for (uint16_t index = 0; index < 65; index++)
                    {
                        uint16_t tex_y = (int)(solid_tiles[index] / 49);
                        uint16_t tex_x = solid_tiles[index] - tex_y*49;
                        Vector2 mouse_pos = GetMousePosition();
                        if (GuiButton((Rectangle){view.x + pos_x*TILE_SIZE*2 + scroll.x, view.y + pos_y*TILE_SIZE*2 + scroll.y, TILE_SIZE*2, TILE_SIZE*2}, "") && 
                            mouse_pos.x <= tile_selection_panel.x + tile_selection_panel.width && 
                            mouse_pos.x >= tile_selection_panel.x && 
                            mouse_pos.y >= tile_selection_panel.y && 
                            mouse_pos.y <= tile_selection_panel.y + tile_selection_panel.height)
                        {
                            selected_texture_id = solid_tiles[index];
                            active_editor_mode = 0;
                        }
                        DrawTexturePro(
                            *tilesheet, 
                            (Rectangle){TILE_SIZE*tex_x, TILE_SIZE*tex_y, TILE_SIZE, TILE_SIZE}, 
                            (Rectangle){view.x + pos_x*TILE_SIZE*2 + scroll.x, view.y +pos_y*TILE_SIZE*2 + scroll.y, TILE_SIZE*2, TILE_SIZE*2}, 
                            (Vector2){0.0f, 0.0f}, 
                            0.0f, 
                            WHITE
                        );
                        pos_x++;
                        if (pos_x == num_tiles_x)
                        {
                            pos_x = 0;
                            pos_y++;
                        }
                    }
                }
                else if (active_tile_category == 1)
                {
                    uint16_t num_tiles_x = floor(tile_scroll_panel.width / (TILE_SIZE*2));
                    uint16_t pos_x = 0;
                    uint16_t pos_y = 0;
                    for (uint16_t index = 0; index < 18; index++)
                    {
                        uint16_t tex_y = (int)(decoration_tiles[index] / 49);
                        uint16_t tex_x = decoration_tiles[index] - tex_y*49;
                        Vector2 mouse_pos = GetMousePosition();
                        if (GuiButton((Rectangle){view.x + pos_x*TILE_SIZE*2 + scroll.x, view.y + pos_y*TILE_SIZE*2 + scroll.y, TILE_SIZE*2, TILE_SIZE*2}, "") && 
                            mouse_pos.x <= tile_selection_panel.x + tile_selection_panel.width && 
                            mouse_pos.x >= tile_selection_panel.x && 
                            mouse_pos.y >= tile_selection_panel.y && 
                            mouse_pos.y <= tile_selection_panel.y + tile_selection_panel.height)
                        {
                            selected_texture_id = decoration_tiles[index];
                            active_editor_mode = 0;
                        }
                        DrawTexturePro(
                            *tilesheet, 
                            (Rectangle){TILE_SIZE*tex_x, TILE_SIZE*tex_y, TILE_SIZE, TILE_SIZE}, 
                            (Rectangle){view.x + pos_x*TILE_SIZE*2 + scroll.x, view.y + pos_y*TILE_SIZE*2 + scroll.y, TILE_SIZE*2, TILE_SIZE*2}, 
                            (Vector2){0.0f, 0.0f}, 
                            0.0f, 
                            WHITE
                        );
                        pos_x++;
                        if (pos_x == num_tiles_x)
                        {
                            pos_x = 0;
                            pos_y++;
                        }
                    }
                }
                else if (active_tile_category == 2)
                {
                    uint16_t num_tiles_x = floor(tile_scroll_panel.width / (TILE_SIZE*2));
                    uint16_t pos_x = 0;
                    uint16_t pos_y = 0;
                    for (uint16_t index = 0; index < 18; index++)
                    {
                        uint16_t tex_y = (int)(enemy_tiles[index] / 49);
                        uint16_t tex_x = enemy_tiles[index] - tex_y*49;
                        Vector2 mouse_pos = GetMousePosition();
                        if (GuiButton((Rectangle){view.x + pos_x*TILE_SIZE*2 + scroll.x, view.y + pos_y*TILE_SIZE*2 + scroll.y, TILE_SIZE*2, TILE_SIZE*2}, "") && 
                            mouse_pos.x <= tile_selection_panel.x + tile_selection_panel.width && 
                            mouse_pos.x >= tile_selection_panel.x && 
                            mouse_pos.y >= tile_selection_panel.y && 
                            mouse_pos.y <= tile_selection_panel.y + tile_selection_panel.height)
                        {
                            selected_texture_id = enemy_tiles[index];
                            active_editor_mode = 0;
                        }
                        DrawTexturePro(
                            *tilesheet, 
                            (Rectangle){TILE_SIZE*tex_x, TILE_SIZE*tex_y, TILE_SIZE, TILE_SIZE}, 
                            (Rectangle){view.x + pos_x*TILE_SIZE*2 + scroll.x, view.y + pos_y*TILE_SIZE*2 + scroll.y, TILE_SIZE*2, TILE_SIZE*2}, 
                            (Vector2){0.0f, 0.0f}, 
                            0.0f, 
                            WHITE
                        );
                        pos_x++;
                        if (pos_x == num_tiles_x)
                        {
                            pos_x = 0;
                            pos_y++;
                        }
                    }
                }
                else if (active_tile_category == 3)
                {
                    uint16_t num_tiles_x = floor(tile_scroll_panel.width / (TILE_SIZE*2));
                    uint16_t pos_x = 0;
                    uint16_t pos_y = 0;
                    for (uint16_t index = 0; index < 10; index++)
                    {
                        uint16_t tex_y = (int)(object_tiles[index] / 49);
                        uint16_t tex_x = object_tiles[index] - tex_y*49;
                        Vector2 mouse_pos = GetMousePosition();
                        if (GuiButton((Rectangle){view.x + pos_x*TILE_SIZE*2 + scroll.x, view.y + pos_y*TILE_SIZE*2 + scroll.y, TILE_SIZE*2, TILE_SIZE*2}, "") && 
                            mouse_pos.x <= tile_selection_panel.x + tile_selection_panel.width && 
                            mouse_pos.x >= tile_selection_panel.x && 
                            mouse_pos.y >= tile_selection_panel.y && 
                            mouse_pos.y <= tile_selection_panel.y + tile_selection_panel.height)
                        {
                            selected_texture_id = object_tiles[index];
                            active_editor_mode = 0;
                        }
                        DrawTexturePro(
                            *tilesheet, 
                            (Rectangle){TILE_SIZE*tex_x, TILE_SIZE*tex_y, TILE_SIZE, TILE_SIZE}, 
                            (Rectangle){view.x + pos_x*TILE_SIZE*2 + scroll.x, view.y + pos_y*TILE_SIZE*2 + scroll.y, TILE_SIZE*2, TILE_SIZE*2}, 
                            (Vector2){0.0f, 0.0f}, 
                            0.0f, 
                            WHITE
                        );
                        pos_x++;
                        if (pos_x == num_tiles_x)
                        {
                            pos_x = 0;
                            pos_y++;
                        }
                    }
                }
                else if (active_tile_category == 4)
                {
                    uint16_t num_tiles_x = floor(tile_scroll_panel.width / (TILE_SIZE*2));
                    uint16_t pos_x = 0;
                    uint16_t pos_y = 0;
                    uint16_t tex_y = (int)(ENTITY_TYPE_PLAYER_START / 49);
                    uint16_t tex_x = ENTITY_TYPE_PLAYER_START - tex_y*49;
                    Vector2 mouse_pos = GetMousePosition();
                    if (GuiButton((Rectangle){view.x + pos_x*TILE_SIZE*2 + scroll.x, view.y + pos_y*TILE_SIZE*2 + scroll.y, TILE_SIZE*2, TILE_SIZE*2}, "") && 
                        mouse_pos.x <= tile_selection_panel.x + tile_selection_panel.width && 
                        mouse_pos.x >= tile_selection_panel.x && 
                        mouse_pos.y >= tile_selection_panel.y && 
                        mouse_pos.y <= tile_selection_panel.y + tile_selection_panel.height)
                    {
                        selected_texture_id = ENTITY_TYPE_PLAYER_START;
                        active_editor_mode = 0;
                    }
                    DrawTexturePro(
                        *tilesheet, 
                        (Rectangle){TILE_SIZE*tex_x, TILE_SIZE*tex_y, TILE_SIZE, TILE_SIZE}, 
                        (Rectangle){view.x + pos_x*TILE_SIZE*2 + scroll.x, view.y + pos_y*TILE_SIZE*2 + scroll.y, TILE_SIZE*2, TILE_SIZE*2}, 
                        (Vector2){0.0f, 0.0f}, 
                        0.0f, 
                        WHITE
                    );
                    pos_x++;
                    if (pos_x == num_tiles_x)
                    {
                        pos_x = 0;
                        pos_y++;
                    }
                }
                else
                {
                    for (uint16_t tile_y = 0; tile_y < num_tiles_vertical; tile_y++)
                    {
                        for (uint16_t tile_x = 0; tile_x < num_tiles_horizontal; tile_x++)
                        {
                            Vector2 mouse_pos = GetMousePosition();
                            if (GuiButton((Rectangle){view.x + tile_x*TILE_SIZE*2 + scroll.x, view.y + tile_y*TILE_SIZE*2 + scroll.y, TILE_SIZE*2, TILE_SIZE*2}, "") && 
                                mouse_pos.x <= tile_selection_panel.x + tile_selection_panel.width && 
                                mouse_pos.x >= tile_selection_panel.x && 
                                mouse_pos.y >= tile_selection_panel.y && 
                                mouse_pos.y <= tile_selection_panel.y + tile_selection_panel.height)
                            {
                                selected_texture_id = tile_x + tile_y * num_tiles_horizontal;
                                active_editor_mode = 0;
                            }
                            DrawTexturePro(
                                *tilesheet, 
                                (Rectangle){TILE_SIZE*tile_x, TILE_SIZE*tile_y, TILE_SIZE, TILE_SIZE}, 
                                (Rectangle){view.x + tile_x*TILE_SIZE*2 + scroll.x, view.y + tile_y*TILE_SIZE*2 + scroll.y, TILE_SIZE*2, TILE_SIZE*2}, 
                                (Vector2){0.0f, 0.0f}, 
                                0.0f, 
                                WHITE
                            );
                        }
                    }
                }
            EndScissorMode();

            GuiSetStyle(BUTTON, BASE_COLOR_NORMAL, 0xc9c9c9ff);
            GuiSetStyle(BUTTON, BASE_COLOR_FOCUSED, 0xc9effeff);
            GuiSetStyle(BUTTON, BORDER_WIDTH, 1);

            // Entity view
            GuiGroupBox(entity_group_box, "entity");
            DrawRectangleRec((Rectangle){entity_group_box.x + 5, entity_group_box.y + 15, TILE_SIZE*2, TILE_SIZE*2}, BLACK);
            DrawTexturePro(
                *tilesheet, 
                (Rectangle){TILE_SIZE*(world_map[selected_entity_y][selected_entity_x].texture_id % num_tiles_horizontal), TILE_SIZE*floor(world_map[selected_entity_y][selected_entity_x].texture_id / num_tiles_horizontal), TILE_SIZE, TILE_SIZE}, 
                (Rectangle){entity_group_box.x + 5, entity_group_box.y + 15, TILE_SIZE*2, TILE_SIZE*2}, 
                (Vector2){0.0f, 0.0f}, 
                0.0f, 
                WHITE
            );
            GuiLabel(entity_type, TextFormat("type: %i", world_map[selected_entity_y][selected_entity_x].type));
            GuiLabel(entity_texture_id, TextFormat("texture_id: %i", world_map[selected_entity_y][selected_entity_x].texture_id));
            GuiLabel(entity_position, TextFormat("position: %i, %i", selected_entity_x, selected_entity_y));
            GuiLabel(entity_rotation, "rotation: ");
            if (world_map[selected_entity_y][selected_entity_x].texture_id != 0 && active_editor_mode == 2)
            {
                GuiComboBox(entity_rotation_combobox, rotation_combobox_options, &active_rotation_option);
            }
            GuiLabel(entity_flipped, "flipped: ");
            if (world_map[selected_entity_y][selected_entity_x].texture_id != 0 && active_editor_mode == 2)
            {
                GuiComboBox(entity_flipped_combobox, flipped_combobox_options, &active_flipped_option);
            }
        }
    EndDrawing();
}

void scene_editor_draw_gui(void)
{

}

void scene_editor_input(void)
{
    if (IsKeyPressed(KEY_ESCAPE))
    {
        if (file_changed)
        {
            unsaved_msg_box = true;
        }
        else
        {
            draw_gui = true;
            game_set_scene(GAME_SCENE_MENU);
            scene_editor_unload_assets();
        }
    }
    if (IsKeyPressed(KEY_TAB))
    {
        draw_gui = !draw_gui;
    }
    if (IsKeyPressed(KEY_D))
    {
        active_editor_mode = 1;
    }
    if (IsKeyPressed(KEY_S))
    {
        active_editor_mode = 2;
    }
    if (IsKeyPressed(KEY_P))
    {
        active_editor_mode = 0;
    }

    if (IsKeyDown(KEY_LEFT_SHIFT))
    {
        delete_mode = true;
    }
    else
    {
        delete_mode = false;
    }

    if (IsMouseButtonDown(MOUSE_BUTTON_MIDDLE))
    {
        camera.offset.x += GetMouseDelta().x;
        camera.offset.y += GetMouseDelta().y;
    }
    if (GetMouseWheelMove() > 0 && GetMousePosition().x > 500)
    {
        camera.zoom += 10.0f * GetFrameTime();
        if (camera.zoom > 3.0f)
        {
            camera.zoom = 3.0f;
        }
    }
    else if (GetMouseWheelMove() < 0 && GetMousePosition().x > 500)
    {
        camera.zoom -= 10.0f * GetFrameTime();
        if (camera.zoom < 1.0f)
        {
            camera.zoom = 1.0f;
        }
    }
}

void scene_editor_unload_assets(void)
{
}