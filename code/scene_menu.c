#include "game.h"

#define MAX_NUM_MENU_OPTIONS 5
const char MENU_TITLE_STRING[20] = "Project Gauntlet";
const char MENU_OPTIONS_STRINGS[MAX_NUM_MENU_OPTIONS][15] = {
    "Play",
    "Settings",
    "Editor",
    "Credits", 
    "Quit"
};
static uint8_t selected_menu_option;

void scene_menu_init(void)
{
    selected_menu_option = 0;
}

void scene_menu_update(void)
{
    scene_menu_input();
}

void scene_menu_render(void)
{
    BeginDrawing();
        ClearBackground(BLACK);
        
        DrawText(MENU_TITLE_STRING, 50, 50, 100, LIGHTGRAY);
        // Draw menu options
        for (uint8_t menu_option = 0; menu_option < MAX_NUM_MENU_OPTIONS; menu_option++)
        {
            DrawText(MENU_OPTIONS_STRINGS[menu_option], 50, GetScreenHeight()-300 + menu_option*50, 50, menu_option == selected_menu_option ? WHITE : GRAY);
        }
    EndDrawing();
}

void scene_menu_input(void)
{
    if (IsKeyPressed(KEY_UP) || IsKeyPressed(KEY_W))
    {
        if (selected_menu_option == 0)
        {
            selected_menu_option = MAX_NUM_MENU_OPTIONS-1;
        }
        else
        {
            selected_menu_option--;
        }
    }
    if (IsKeyPressed(KEY_DOWN) || IsKeyPressed(KEY_S))
    {
        if (selected_menu_option == MAX_NUM_MENU_OPTIONS-1)
        {
            selected_menu_option = 0;
        }
        else
        {
            selected_menu_option++;
        }
    }
    if (IsKeyPressed(KEY_ENTER))
    {
        // Change scene
        if (selected_menu_option == 0)
            game_set_scene(GAME_SCENE_PLAY);
        else if (selected_menu_option == 1)
            game_set_scene(GAME_SCENE_SETTINGS);
        else if (selected_menu_option == 2)
            game_set_scene(GAME_SCENE_EDITOR);
        else if (selected_menu_option == 3)
            game_set_scene(GAME_SCENE_CREDITS);
        else if (selected_menu_option == 4)
            game_stop_running();
    }
}