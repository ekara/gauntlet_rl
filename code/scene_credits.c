#include "game.h"

static uint8_t selected_menu_option;

void scene_credits_init(void)
{
    selected_menu_option = 0;
}

void scene_credits_update(void)
{
    scene_credits_input();
}

void scene_credits_render(void)
{
    BeginDrawing();
        ClearBackground(BLACK);
        
        DrawText("Credits", 50, 50, 100, LIGHTGRAY);
        DrawText("Ertugrul Kara", 50, 300, 50, LIGHTGRAY);
    EndDrawing();
}

void scene_credits_input(void)
{
    if (IsKeyPressed(KEY_ESCAPE))
    {
        game_set_scene(GAME_SCENE_MENU);
    }
}
