#include "entity.h" 

EntityType solid_tiles_2[65] = {
    ENTITY_TYPE_SOLID_TREE_1,
    ENTITY_TYPE_SOLID_TREE_2,
    ENTITY_TYPE_SOLID_TREE_3,
    ENTITY_TYPE_SOLID_TREE_4,
    ENTITY_TYPE_SOLID_TREE_5,
    ENTITY_TYPE_SOLID_TREE_6,
    ENTITY_TYPE_SOLID_TREE_7,
    ENTITY_TYPE_SOLID_TREE_8,
    ENTITY_TYPE_SOLID_TREE_9,
    ENTITY_TYPE_SOLID_TREE_10,
    ENTITY_TYPE_SOLID_TREE_11,
    ENTITY_TYPE_SOLID_TREE_12,
    ENTITY_TYPE_SOLID_TREE_13,
    ENTITY_TYPE_SOLID_TREE_14,
    ENTITY_TYPE_SOLID_TREE_15,
    ENTITY_TYPE_SOLID_STONE,
    ENTITY_TYPE_SOLID_WATER_1,
    ENTITY_TYPE_SOLID_WATER_2,
    ENTITY_TYPE_SOLID_WATER_3,
    ENTITY_TYPE_SOLID_WATER_4,
    ENTITY_TYPE_SOLID_WATER_5,
    ENTITY_TYPE_SOLID_WATER_6,
    ENTITY_TYPE_SOLID_WATER_7,
    ENTITY_TYPE_SOLID_WATER_8,
    ENTITY_TYPE_SOLID_WATER_9,
    ENTITY_TYPE_SOLID_PIPE_1,
    ENTITY_TYPE_SOLID_PIPE_2,
    ENTITY_TYPE_SOLID_PIPE_3,
    ENTITY_TYPE_SOLID_PIPE_4,
    ENTITY_TYPE_SOLID_WALL_1,
    ENTITY_TYPE_SOLID_WALL_2,
    ENTITY_TYPE_SOLID_WALL_3,
    ENTITY_TYPE_SOLID_WALL_4,
    ENTITY_TYPE_SOLID_WALL_5,
    ENTITY_TYPE_SOLID_WALL_6,
    ENTITY_TYPE_SOLID_WALL_7,
    ENTITY_TYPE_SOLID_WALL_8,
    ENTITY_TYPE_SOLID_WALL_9,
    ENTITY_TYPE_SOLID_WALL_10,
    ENTITY_TYPE_SOLID_WALL_11,
    ENTITY_TYPE_SOLID_WALL_12,
    ENTITY_TYPE_SOLID_WALL_13,
    ENTITY_TYPE_SOLID_WALL_14,
    ENTITY_TYPE_SOLID_WALL_15,
    ENTITY_TYPE_SOLID_WALL_16,
    ENTITY_TYPE_SOLID_WALL_17,
    ENTITY_TYPE_SOLID_WALL_18,
    ENTITY_TYPE_SOLID_WALL_19,
    ENTITY_TYPE_SOLID_WALL_20,
    ENTITY_TYPE_SOLID_WALL_21,
    ENTITY_TYPE_SOLID_WALL_22,
    ENTITY_TYPE_SOLID_WALL_23,
    ENTITY_TYPE_SOLID_WALL_24,
    ENTITY_TYPE_SOLID_WALL_25,
    ENTITY_TYPE_SOLID_WALL_26,
    ENTITY_TYPE_SOLID_WALL_27,
    ENTITY_TYPE_SOLID_WALL_28,
    ENTITY_TYPE_SOLID_WALL_29,
    ENTITY_TYPE_SOLID_WALL_30,
    ENTITY_TYPE_SOLID_WALL_31,
    ENTITY_TYPE_SOLID_GRAVE_1,
    ENTITY_TYPE_SOLID_GRAVE_1,
    ENTITY_TYPE_SOLID_SIGN_1,
    ENTITY_TYPE_SOLID_SIGN_2,
    ENTITY_TYPE_SOLID_SIGN_1
};

EntityType decoration_tiles_2[18] = {
    ENTITY_TYPE_DECORATION_GROUND_1,
    ENTITY_TYPE_DECORATION_GROUND_2,
    ENTITY_TYPE_DECORATION_GROUND_3,
    ENTITY_TYPE_DECORATION_GROUND_4,
    ENTITY_TYPE_DECORATION_GRASS_1,
    ENTITY_TYPE_DECORATION_GRASS_2,
    ENTITY_TYPE_DECORATION_GRASS_3,
    ENTITY_TYPE_DECORATION_STREET_1,
    ENTITY_TYPE_DECORATION_STREET_2,
    ENTITY_TYPE_DECORATION_STREET_3,
    ENTITY_TYPE_DECORATION_STREET_4,
    ENTITY_TYPE_DECORATION_STREET_5,
    ENTITY_TYPE_DECORATION_BONES_1,
    ENTITY_TYPE_DECORATION_BONES_2,
    ENTITY_TYPE_DECORATION_WEB,
    ENTITY_TYPE_DECORATION_TORCH_1,
    ENTITY_TYPE_DECORATION_TORCH_2,
    ENTITY_TYPE_DECORATION_TORCH_3
};

EntityType object_tiles_2[10] = {
    ENTITY_TYPE_OBJECT_FOOD_1,
    ENTITY_TYPE_OBJECT_FOOD_2,
    ENTITY_TYPE_OBJECT_FOOD_3,
    ENTITY_TYPE_OBJECT_FOOD_4,
    ENTITY_TYPE_OBJECT_FOOD_5,
    ENTITY_TYPE_OBJECT_FOOD_6,
    ENTITY_TYPE_OBJECT_FOOD_7,
    ENTITY_TYPE_OBJECT_JEWEL_1,
    ENTITY_TYPE_OBJECT_JEWEL_2,
    ENTITY_TYPE_OBJECT_JEWEL_3
};

EntityType enemy_tiles_2[18] = {
    ENTITY_TYPE_ENEMY_SCORPION,
    ENTITY_TYPE_ENEMY_CRAB,
    ENTITY_TYPE_ENEMY_BEE,
    ENTITY_TYPE_ENEMY_TURTLE,
    ENTITY_TYPE_ENEMY_SPIDER_1,
    ENTITY_TYPE_ENEMY_SPIDER_2,
    ENTITY_TYPE_ENEMY_SPIDER_3,
    ENTITY_TYPE_ENEMY_GHOST_1,
    ENTITY_TYPE_ENEMY_GHOST_2,
    ENTITY_TYPE_ENEMY_SKELETON,
    ENTITY_TYPE_ENEMY_GOLEM,
    ENTITY_TYPE_ENEMY_SKULL,
    ENTITY_TYPE_ENEMY_OCTOPUS,
    ENTITY_TYPE_ENEMY_BAT,
    ENTITY_TYPE_ENEMY_SNAKE,
    ENTITY_TYPE_ENEMY_ALLIGATOR,
    ENTITY_TYPE_ENEMY_BEAR,
    ENTITY_TYPE_ENEMY_MAGE
};

// void entity_create(Entity *entities, uint32_t *num_entities, bool active, EntityType type, uint16_t texture_id, float texture_flipped, Vector2 size, Vector2 position, Vector2 velocity, float health, float lifespan, float rotation)
// {
//     entities[*num_entities].active = true;
//     entities[*num_entities].id = 0;
//     entities[*num_entities].type = ENTITY_TYPE_PLAYER_WIZARD;
//     entities[*num_entities].state = ENTITY_STATE_IDLE;
//     entities[*num_entities].texture_id = 73;
//     entities[*num_entities].texture_flipped = 1.0f;
//     entities[*num_entities].size = (Vector2){.x = TILE_SIZE, .y = TILE_SIZE};
//     entities[*num_entities].origin = (Vector2){.x = entities[*num_entities].size.x*0.5f, .y = entities[*num_entities].size.y*0.5f};
//     entities[*num_entities].spawn_position = (Vector2){50.0f*16, 95.0f*16};
//     entities[*num_entities].position = entities[*num_entities].spawn_position;
//     entities[*num_entities].previous_position = entities[*num_entities].position;
//     entities[*num_entities].rotation = 0.0f;
//     entities[*num_entities].velocity = (Vector2){PLAYER_SPEED, PLAYER_SPEED};
//     entities[*num_entities].collision_rect = (Rectangle) {.x = entities[*num_entities].position.x, .y = entities[*num_entities].position.y, .width = TILE_SIZE, .height = TILE_SIZE};
//     entities[*num_entities].health = 100.0f;
//     entities[*num_entities].lifespan = 0.0f;
// }

void entity_draw(Texture2D *tilesheet, Entity *entity)
{
    uint16_t tex_y = (int)(entity->texture_id / 49);
    uint16_t tex_x = entity->texture_id - tex_y*49;
    Rectangle source = {.x = tex_x * TILE_SIZE, .y = tex_y*TILE_SIZE, .width = TILE_SIZE * entity->texture_flipped, .height = TILE_SIZE};
    Rectangle dest = {.x = entity->position.x+entity->origin.x, .y = entity->position.y+entity->origin.y, .width = TILE_SIZE, .height = TILE_SIZE};
    Vector2 origin = {entity->origin.x, entity->origin.y};
    DrawTexturePro(*tilesheet, source, dest, origin, entity->rotation, WHITE);
}

bool entity_is_tile(EntityType type)
{
    for (int i = 0; i < 65; i++)
    {
        if (type == solid_tiles_2[i])
        {
            return true;
        }
    }
    return false;
}

bool entity_is_decoration(EntityType type)
{
    for (int i = 0; i < 65; i++)
    {
        if (type == decoration_tiles_2[i])
        {
            return true;
        }
    }
    return false;
}

bool entity_is_enemy(EntityType type)
{
    for (int i = 0; i < 65; i++)
    {
        if (type == enemy_tiles_2[i])
        {
            return true;
        }
    }
    return false;
}

bool entity_is_object(EntityType type)
{
    for (int i = 0; i < 65; i++)
    {
        if (type == object_tiles_2[i])
        {
            return true;
        }
    }
    return false;
}