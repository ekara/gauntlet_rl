#ifndef GAME_H
#define GAME_H

#include <stdint.h>

#include "raylib.h"

#include "entity.h"

typedef enum GameScene
{
    GAME_SCENE_MENU = 0,
    GAME_SCENE_PLAY,
    GAME_SCENE_SETTINGS,
    GAME_SCENE_CREDITS,
    GAME_SCENE_EDITOR,
    
    SCENE_TYPE_MAX
} GameScene;

typedef enum GameResolution
{
    GAME_RESOLUTION_HD = 0,
    GAME_RESOLUTION_FHD,
    // GAME_RESOLUTION_QHD,
    // GAME_RESOLUTION_UHD,

    GAME_RESOLUTION_MAX
} GameResolution;

#define MAX_NUM_PLAYERS 4
#define MAX_MAP_WIDTH 100
#define MAX_MAP_HEIGHT 100
#define MAX_NUM_ENTITIES MAX_MAP_WIDTH * MAX_MAP_HEIGHT

typedef struct Game
{
    uint32_t framecount;
    uint8_t volume;
    bool running;
    bool fullscreen;
    GameResolution game_resolution;
    GameScene current_scene;
    GameScene next_scene;
    bool switch_scene;
    Texture2D tilesheet;
    uint8_t current_level;
    // Entity entites[MAX_NUM_ENTITIES];
    // uint32_t num_of_entities;
    // Entity *players[MAX_NUM_PLAYERS];
    // uint32_t num_of_players;
    // Entity *enemies[MAX_NUM_ENTITIES];
    // uint32_t num_of_enemies;
    // Entity *world_map[MAX_MAP_HEIGHT][MAX_MAP_WIDTH];
} Game;

typedef struct LevelEditor
{
    uint8_t selected_menu_option;
    uint16_t selected_texture_id;
    bool draw_gui;
    bool draw_grid;
    bool draw_textures;
    bool draw_collision;
} LevelEditor;

void game_init(void);
void game_run(void);
void game_update(void);
void game_render(void);
void game_quit(void);
void game_unload_assets(void);
void game_stop_running(void);
void game_set_fullscreen(void);
void game_set_scene(GameScene scene_type);
void game_set_resolution(GameResolution resolution);

// All function related to the menu scene
void scene_menu_init(void);
void scene_menu_update(void);
void scene_menu_render(void);
void scene_menu_input(void);
void scene_menu_do_action(void);
void scene_menu_set_paused(void);

// All function related to the player select scene
void scene_player_select_init(void);
void scene_player_select_update(void);
void scene_player_select_render(void);
void scene_player_select_input(void);
void scene_player_select_set_paused(void);

// All function related to the gameplay_scene
void scene_play_init(void);
void scene_play_update(void);
void scene_play_render(Texture2D *tilesheet);
void scene_play_input(void);
void scene_play_set_paused(void);
void scene_play_unload_assets(void);

// All function related to the settings scene
void scene_settings_init(void);
void scene_settings_update(Game *game);
void scene_settings_render(Game *game);
void scene_settings_input(Game *game);
void scene_settings_set_paused(void);

// All function related to the editor scene
void scene_editor_init(void);
void scene_editor_update(void);
void scene_editor_render(Texture2D *tilesheet);
void scene_editor_input(void);
void scene_editor_set_paused(void);
void scene_editor_unload_assets(void);

// All function related to the credits scene
void scene_credits_init(void);
void scene_credits_update(void);
void scene_credits_render(void);
void scene_credits_input(void);
void scene_credits_set_paused(void);

#endif // GAME_H