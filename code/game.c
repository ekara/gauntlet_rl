#include "game.h"

Game game = {0};

void game_init(void)
{
    game.current_scene = GAME_SCENE_MENU;
    game.next_scene = GAME_SCENE_MENU;
    game.switch_scene = false;
    game.running = true;
    game.framecount = 0;
    game.fullscreen = false;
    game.volume = 100;
    game.game_resolution = GAME_RESOLUTION_HD;

    // Raylib window setup
    // SetConfigFlags(FLAG_WINDOW_RESIZABLE);
    uint16_t window_width = 1280;
    uint16_t window_height = 720;
    InitWindow(window_width, window_height, "Gauntlet Project");
    game_set_resolution(game.game_resolution);
    SetTargetFPS(60);
    SetExitKey(0); // prevents ESCAPE from closing the whole game
    
    if (game.fullscreen)
    {
        game_set_fullscreen();
    }

    game.tilesheet = LoadTexture("../data/images/colored-transparent_packed.png");
}

void game_set_resolution(GameResolution resolution)
{
    uint16_t window_width = 0;
    uint16_t window_height = 0;
    if (resolution == GAME_RESOLUTION_HD)
    {
        window_width = 1280;
        window_height = 720;
    }
    else if (resolution == GAME_RESOLUTION_FHD)
    {
        window_width = 1920;
        window_height = 1080;
    }
    // else if (resolution == GAME_RESOLUTION_QHD)
    // {
    //     window_width = 2560;
    //     window_height = 1440;
    // }
    // else if (resolution == GAME_RESOLUTION_UHD)
    // {
    //     window_width = 3840;
    //     window_height = 2160;
    // }
    game.game_resolution = resolution;

    if (IsWindowFullscreen())
    {
        ToggleFullscreen();
        SetWindowSize(window_width, window_height);
        SetWindowPosition(
            (GetMonitorWidth(GetCurrentMonitor()) - window_width)*0.5f, 
            (GetMonitorHeight(GetCurrentMonitor()) - window_height)*0.5f
        );
        ToggleFullscreen();
    }
    else
    {
        SetWindowSize(window_width, window_height);
        SetWindowPosition(
            (GetMonitorWidth(GetCurrentMonitor()) - window_width)*0.5f, 
            (GetMonitorHeight(GetCurrentMonitor()) - window_height)*0.5f
        );
    }
}

void game_run(void)
{
    game_init();
    while(game.running)
    {
        if (WindowShouldClose())
        {
            game.running = false;
        }

        game_update();
        game_render();
    }
    game_quit();
}

void game_update(void)
{
    switch(game.current_scene)
    {
        case GAME_SCENE_MENU: { scene_menu_update(); } break;
        case GAME_SCENE_PLAY: { scene_play_update(); } break;
        case GAME_SCENE_SETTINGS: { scene_settings_update(&game); } break;
        case GAME_SCENE_EDITOR: { scene_editor_update(); } break;
        case GAME_SCENE_CREDITS: { scene_credits_update(); } break;
        default: {} break;
    }
}

void game_render(void)
{
    switch(game.current_scene)
    {
        case GAME_SCENE_MENU: { scene_menu_render(); } break;
        case GAME_SCENE_PLAY: { scene_play_render(&game.tilesheet); } break;
        case GAME_SCENE_SETTINGS: { scene_settings_render(&game); } break;
        case GAME_SCENE_EDITOR: { scene_editor_render(&game.tilesheet); } break;
        case GAME_SCENE_CREDITS: { scene_credits_render(); } break;
        default: {} break;
    }
    game.framecount++;
}

void game_set_scene(GameScene scene_type)
{
    game.current_scene = scene_type;
    switch (scene_type)
    {
        case GAME_SCENE_MENU: { scene_menu_init(); } break;
        case GAME_SCENE_PLAY: { scene_play_init(); } break;
        case GAME_SCENE_SETTINGS: { scene_settings_init(); } break;
        case GAME_SCENE_EDITOR: { scene_editor_init(); } break;
        case GAME_SCENE_CREDITS: { scene_credits_init(); } break;
        default: {} break;
    }
}

void game_set_fullscreen(void)
{
    if (game.fullscreen)
    {
        SetWindowSize(1280, 720);
    }
    else
    {
        SetWindowSize(GetMonitorWidth(GetCurrentMonitor()), GetMonitorHeight(GetCurrentMonitor()));
    }
    ToggleFullscreen();
    game.fullscreen = !game.fullscreen;
}

void game_unload_assets(void)
{
    UnloadTexture(game.tilesheet);
}

void game_stop_running(void)
{
    game.running = false;
}

void game_quit(void)
{
    game_unload_assets();
    CloseWindow();
}