#ifndef CAMERA_H
#define CAMERA_H

#include "entity.h"

#include "raylib.h"

typedef enum CameraType
{
    CAMERA_TYPE_FOLLOW = 0,

    CAMERA_TYPE_MAX
} CameraType;

void camera_init(Camera2D *camera, Entity *target, float zoom, CameraType type);
void camera_update(Camera2D *camera, Entity *target, float world_width, float world_height);

#endif // CAMERA_H