# gauntlet_rl

A 2D top-down game inspired by Gauntlet. The game is built in C with raylib.

## Description

The goal is to play through a set of floors to escape a tower. Each floor has enemies to kill and a reward at the end.
Each run through the tower consists of a random selection of handmade floors. The player can pick one of four characters to play with.

## Planned Features

- Local 4 player co-op
- Integrated tile based level editor
- Nintendo Switch support

## Used Assets

- Tilesheet: https://kenney-assets.itch.io/1-bit-pack

## Screenshots

![gauntlet_rl_play](screenshots/gauntlet_rl_play.png)

![gauntlet_rl_editor](screenshots/gauntlet_rl_editor.png)